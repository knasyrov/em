module EM
  module S3
    ##############  конфиг
    class << self
      attr_accessor :configuration

      def configure 
        self.configuration ||= Configuration.new
        yield(configuration)
      end      
    end

    class Configuration
      attr_accessor :aws_access_key_id, :aws_secret_access_key, :server, :bucket
    end

    class UploadError < RuntimeError; end
    class TypeMismatchError < RuntimeError; end

  end
end