require 'eventmachine'
require 'happening'
require 'fastimage'
require 'mini_magick'
require 'nokogiri'
require 'em-http'
require 'em-http/middleware/json_response'
require 'tempfile'

require 'em/s3/virtual_version'

module EM
  module S3
     class BaseUploader
      include EM::S3::VirtualVersions

      class << self
        def converter
          unless @converter
            uri = URI.parse(Wannafun.config['image_converter']['url'])
            @converter = {
              host: uri.host,
              port: (uri.port == 80 ? nil : uri.port)
            }
          end
          @converter
        end

        def encode_key
          @encode_key ||= Wannafun.config['image_converter']['key']
        end

        def amazon_host
          @amazon_host ||= Happening::S3::Item.new(
            S3.configuration.bucket, '/', 
            server: S3.configuration.server,
            permissions: 'public-read',
            protocol: 'http'
          ).server
        end

        def encode_path(json)
          JWT.encode(json.to_json, encode_key, 'HS256')
        end

        def converter_url model, field, size, method = 'avatar'
          source = {url: store_url(model, field)}
          source.merge!(crop: model.crop) if model.has_attribute?(:crop)
          source.merge!(rotate: ((model.rotation.to_i)/90%4)) if model.has_attribute?(:rotation)

          params = {params: encode_path(source)}
          params[:size] = size if size

          URI::Generic.new('http', nil, converter[:host], converter[:port], nil, "/#{method}/", nil, params.to_param, nil).to_s
        end

        def store_url model, field
          URI::Generic.new('http', nil, amazon_host, nil, nil, store_path(model, field), nil, nil, nil).to_s
        end
        
        def store_path model, field
          "/uploads/#{model.class.to_s.underscore}/#{field.to_s}/#{model.id.to_s.rjust(8,'0').scan(/../).join('/')}/#{model.try(field)}"
        end
      end

      def initialize model, field
        @model, @field = model, field
      end


      def copy_from source_url
        deferred = EM::DefaultDeferrable.new
        filename = source_url.split('/').last
        file_ext = filename.split('.').last

        item = Happening::S3::Item.new(
          S3.configuration.bucket, 
          "#{store_dir}/#{filename}", 
          aws_access_key_id: S3.configuration.aws_access_key_id,
          aws_secret_access_key: S3.configuration.aws_secret_access_key,
          server: S3.configuration.server,
          protocol: 'http',
          permissions: 'public-read',
          timeout: 30
        )    

        on_error = Proc.new { |response| 
          deferred.fail
        }   

        source_path = URI.parse(source_url).path.last(-1)

        item.put(nil,  
          on_error: on_error,
          headers: {
              'Content-Type' => "image/#{file_ext}",
              'x-amz-copy-source' => "#{S3.configuration.bucket}/#{CGI::escape(source_path)}"
            }
          ) do |response|
            deferred.succeed
        end  
        deferred          
      end

 
      def new_upload uploaded, new_name = false 
        deferred = EM::DefaultDeferrable.new

        download_file_if_needed(uploaded).callback do |temp|
          @original_file = temp[:tempfile]
          @original_type = FastImage.type(@original_file).to_s

          @filename = store_name(@original_type, new_name)

          manipulate!(@original_file.path) do |img|
            prepare @original_file.path
            img.auto_orient
            @width, @height = img['width'], img['height']
            img
          end

            # загружаем оригинал
          s3_upload("#{store_dir}/#{@filename}", @original_file).callback do |rsp|
            rsp[:meta] = {'width' => @width.to_s, 'height' => @height.to_s}

            if virtual_versions
              virtual_versions.each do |k, h|
                if h.key?(:meta)
                  if h[:meta].is_a? Proc
                    h = instance_exec(&h[:meta])
                    rsp[:meta].merge!("#{k}_width" => h[:width],  "#{k}_height" => h[:height])
                  else
                    rsp[:meta].merge!("#{k}_width" => h[:meta][:width],  "#{k}_height" => h[:meta][:height])
                  end
                end
              end
            end

            deferred.succeed(rsp)
          end.errback do |err|
            deferred.fail(err)
          end

        end.errback do |error|
          deferred.fail(error)
        end

        deferred        
      end

      protected

      def s3_upload s3_path, source_path
        deferred = EM::DefaultDeferrable.new

        file_name = s3_path.split('/').last
        file_ext = file_name.split('.').last

        item = Happening::S3::Item.new(
          S3.configuration.bucket, 
          s3_path, 
          aws_access_key_id: S3.configuration.aws_access_key_id,
          aws_secret_access_key: S3.configuration.aws_secret_access_key,
          server: S3.configuration.server,
          protocol: 'http',
          permissions: 'public-read',
          timeout: 30
        )

        on_error = Proc.new { |response| 
          xml = Nokogiri::XML.parse(response.response)
          error_message = xml.css('Error Message').text
          deferred.fail(error_message)
        }   

        item.put(
          File.read(source_path),  
          on_error: on_error,
          headers: {
              'Content-Type' => "image/#{file_ext}" #uploaded_file.content_type
            }
          ) do |response|
            deferred.succeed({url: URI.decode(item.url), filename: file_name})
        end  
        deferred
      end


      def download_file remote_url
        deferred = EM::DefaultDeferrable.new

        download_uri = URI.parse(remote_url)
        file_name = download_uri.path.split('/').last
        file = Tempfile.new(file_name)
        file.binmode

        download_url_no_userinfo = "#{download_uri.scheme}://#{download_uri.host}#{download_uri.path}"
        download_url_no_userinfo += "?#{download_uri.query}" if download_uri.query

        http = EM::HttpRequest.new(download_url_no_userinfo)
        req = http.get(:redirects => 1, :head => download_uri.user.nil? ? {}: {'authorization' => [download_uri.user, download_uri.password]})
        req.callback {
          file.close
          unless req.response_header.status == 200
            file.unlink
            deferred.fail('error')
          else
            file.open
            deferred.succeed(tempfile: file)
          end
        }
        req.stream do |chunk|
          file.write(chunk)
        end
        req.errback do |error|
          file.close
          file.unlink
          deferred.fail(error)
        end
        deferred
      end

      def download_file_if_needed image
        deferred = EM::DefaultDeferrable.new
        if image.is_a?(Hash)
          if image["remote_#{@field}_url".to_sym]
            download_file(image["remote_#{@field}_url".to_sym]).callback do |hash|
              deferred.succeed hash
            end.errback do |error|
              deferred.fail(error)
            end 
          elsif image[@field.to_sym]
            deferred.succeed(tempfile: image[@field.to_sym].tempfile)
          end
        else
          deferred.succeed(tempfile: image.tempfile)
        end
        deferred
      end

      def prepare image_path
      end

      def manipulate!(image_path)
        image = ::MiniMagick::Image.open(image_path)
        image = yield(image)
        image.write(image_path)
        image.destroy!
      end

      def sizes_to_fit(sw, sh, width, height)
        factor_w = sw/width.to_f
        factor_h = sh/height.to_f
        factor = (factor_w > factor_h ? factor_w : factor_h)
        {width: (sw/factor).round, height: (sh/factor).round}
      end

      def store_dir
        "uploads/#{@model.class.to_s.underscore}/#{@field.to_s}/#{@model.id.to_s.rjust(8,'0').scan(/../).join('/')}"
      end

      def types_white_list
        %w(jpg jpeg png wp webp)
      end

      def store_name type, new_name
        if @model && @model.read_attribute(@field).present? && !new_name
          @model.read_attribute(@field)
        else
          "#{secure_token}.#{type}"
        end
      end

      def secure_token
        var = :"@#{@field}_secure_token"
        @model.instance_variable_get(var) or @model.instance_variable_set(var, SecureRandom.hex(6))
      end
    end

  end
end
