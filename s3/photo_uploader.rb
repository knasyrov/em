require 'em/s3/base_uploader'

module EM
  module S3
    class PhotoUploader < BaseUploader

      MINI_SIZE = 's40'
      SMALL_SIZE = 's100'
      THUMB_SIZE = 's150'
      LARGE_SIZE = 's600_380'  

      virtual_version :mini, id: MINI_SIZE, meta: {width: 40, height: 40}
      virtual_version :small, id: SMALL_SIZE, meta: {width: 100, height: 100}
      virtual_version :thumb, id: THUMB_SIZE, meta: {width: 150, height: 150}
      virtual_version :large, id: LARGE_SIZE, meta: proc {sizes_to_fit(@width, @height, 600, 380)}

      protected

      def prepare image_path
        manipulate!(image_path) do |img|
          img.combine_options do |c|
            c.push '+profile'
            c.push '"*"'
          end
          img
        end
      end

    end
  end
end
