module EM
  module S3

    module VirtualVersions
      extend ActiveSupport::Concern

      included do
        class_attribute :virtual_versions
      end

      module ClassMethods 
        def virtual_version name, options = {}
          self.virtual_versions ||= {}
          name = name.to_sym
          return if virtual_versions[name]
          virtual_versions[name] = options

          #define_singleton_method "__#{name}_url" do |model|
          #  converter_url model, options[:id]
          #end
        end
      end
    end

  end
end