require 'em/s3/base_uploader'

module EM
  module S3
    class AvatarUploader < BaseUploader

      THUMB_SIZE = 's40'
      MATRIX_SIZE = 's150'
      PROFILE_SIZE = 's170'

      virtual_version :thumb,   id: THUMB_SIZE
      virtual_version :matrix,  id: MATRIX_SIZE
      virtual_version :profile, id: PROFILE_SIZE
      
      protected

      def prepare image_path
        manipulate!(image_path) do |img|

          img.auto_orient
          img = yield(img) if block_given?
          
          img.combine_options do |c|
            c.push '+profile'
            c.push '"*"'
          end

          width = img['width']
          height = img['height']

          if width < 300 || height < 300
            img.combine_options do |c|
              c.resize '300x300^'
              c.filter 'Quadratic'
            end
          end

          @model.rescale_crop_to img['width'], img['height']
          img
        end    
      end      

    end
  end
end
